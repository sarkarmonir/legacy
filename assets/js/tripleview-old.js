function navigate( container, dir, isAnimating, current, sectionsCount, sections, links, transEndEventName ) {
		if( isAnimating ) {
				return false;
		}
		// return;
		isAnimating = true;
		var invDir = dir === 'right' ? 'left' : 'right';
		classie.add( container, 'qt-vs-move-' + invDir );

		var l = current === 0 ? sectionsCount - 1 : current - 1,
			r = current < sectionsCount - 1 ? current + 1 : 0,
			nextE;
		if( dir === 'right' ) {
			nextE = r < sectionsCount - 1 ? r + 1 : 0;
		}
		else if( dir === 'left' ) {
			nextE = l > 0 ? l - 1 : sectionsCount - 1;
		}
		var nextSection = sections[ nextE ],
			nextLink = links[ nextE ];
		nextSection.className = '';
		nextLink.className = '';
		classie.add( nextSection, 'qt-vs-' + dir + '-outer' );
		classie.add( nextLink, 'qt-vs-nav-' + dir + '-outer' );
		var onTransitionEndFn = function() {
			nextSection.removeEventListener( transEndEventName, onTransitionEndFn );
			cleanClasses(sections, links);
			if( dir === 'right' ) {
				classie.add( sections[ current ], 'qt-vs-left' );
				classie.add( sections[ r ], 'qt-vs-current' );
				classie.add( nextSection, 'qt-vs-right' );
				classie.add( links[ current ], 'qt-vs-nav-left' );
				classie.add( links[ r ], 'qt-vs-nav-current' );
				classie.add( nextLink, 'qt-vs-nav-right' );
				current = current < sectionsCount - 1 ? current + 1 : 0;
			}
			else if( dir === 'left' ) {
				classie.add( nextSection, 'qt-vs-left' );
				classie.add( sections[ l ], 'qt-vs-current' );
				classie.add( sections[ current ], 'qt-vs-right' );
				classie.add( nextLink, 'qt-vs-nav-left' );
				classie.add( links[ l ], 'qt-vs-nav-current' );
				classie.add( links[ current ], 'qt-vs-nav-right' );
				current = current > 0 ? current - 1 : sectionsCount - 1;
			}
			classie.remove( container, 'qt-vs-move-' + invDir );
			isAnimating = false;
		}
		nextSection.addEventListener( transEndEventName, onTransitionEndFn );
	}

	$.fn.qtInitTripleView = function () {
		if($("#qtvscontainer").length === 0){
			return;
		}
		if($("#qtvscontainer section").length === 0){
			return;
		}
		var container = document.getElementById( 'qtvscontainer' ),
			thecontainer = $("#qtvscontainer"),
			containerselector = '#thecontainer',
			wrapper = container.querySelector( 'div.qt-vs-wrapper' ),
			sections = Array.prototype.slice.call( wrapper.querySelectorAll( 'section' ) ),
			links = Array.prototype.slice.call( container.querySelectorAll( 'header.qt-vs-header ul.qt-vs-nav > li' ) ),
			sectionsCount = sections.length,
			transEndEventNames = {
				'WebkitTransition': 'webkitTransitionEnd',
				'MozTransition': 'transitionend',
				'OTransition': 'oTransitionEnd',
				'msTransition': 'MSTransitionEnd',
				'transition': 'transitionend'
			},
			transEndEventName = transEndEventNames[Modernizr.prefixed( 'transition' )],
			eventtype = mobilecheck() ? 'touchstart' : 'click';

		// add navigation elements
		if( sectionsCount >= 3 && Modernizr.csstransforms3d ) {
			var current = 0,
				isAnimating = false;
			thecontainer.addClass('qt-vs-triplelayout');
			
			// assign the current, left and right classes to the respective sections
			classie.add( sections[ current ], 'qt-vs-current' );
			classie.add( sections[ current + 1 ], 'qt-vs-right' );
			classie.add( sections[ sectionsCount - 1 ], 'qt-vs-left' );
			// same for the header links
			classie.add( links[ current ], 'qt-vs-nav-current' );
			classie.add( links[ current + 1 ], 'qt-vs-nav-right' );
			classie.add( links[ sectionsCount - 1 ], 'qt-vs-nav-left' );
			



			thecontainer.on("click", "#qtvtL", function(){
					navigate(container, 'left', isAnimating, current , sectionsCount, sections, links, transEndEventName)
			}).on("click", "#qtvtR", function(){
					navigate( container, 'right', isAnimating, current , sectionsCount, sections, links, transEndEventName);
			});


			links.forEach( function( el, i ) {
				el.addEventListener( eventtype, function( ev ) {
					ev.preventDefault();
					if( classie.has( el, 'qt-vs-nav-right' ) ) {
						navigate( container, 'right', isAnimating, current , sectionsCount, sections, links, transEndEventName);
					}
					else if ( classie.has( el, 'qt-vs-nav-left' ) ) {
						navigate( container, 'left', isAnimating, current , sectionsCount, sections, links, transEndEventName);
					}
					else {
						return false;
					}
				});
			} );
			
			// add keyboard events
			document.addEventListener( 'keydown', function( ev ) {
				var keyCode = ev.keyCode || ev.which,
					arrow = {
						left: 37,
						right: 39
					};
				switch (keyCode) {
					case arrow.left :
						navigate( container, 'left', isAnimating, current , sectionsCount, sections, links, transEndEventName);
						break;
					case arrow.right :
						navigate( container, 'right', isAnimating, current , sectionsCount, sections, links, transEndEventName);
						break;
				}
			} );
			// add swipe events
			if( mobilecheck() ) {
				Hammer( container ).on( 'swipeleft', function( ev ) {
					navigate( container, 'right', isAnimating, current , sectionsCount, sections, links, transEndEventName);
				});
				Hammer( container ).on( 'swiperight', function( ev ) {
					navigate(container, 'left', isAnimating, current , sectionsCount, sections, links, transEndEventName)
				});
			}
			/**
			 * While we load a tripleview page, it may appear smashed for a while, so is hidden via CSS until complete loading
			 */
			$(".qt-vs-container").addClass("active");
		} 
	}